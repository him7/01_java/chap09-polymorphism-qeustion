package com.ohgiraffers.hw1.run;

import com.ohgiraffers.hw1.model.dto.*;

public class Application {

	public static void main(String[] args) {
//		PointDTO circle1 = new CircleDTO(1, 2, 3);
//		PointDTO circle2 = new CircleDTO(3, 3, 4);
//	
//		PointDTO rectangle1 = new RectangleDTO(-1, -2, 5, 2); 
//		PointDTO rectangle2 = new RectangleDTO(-2, 5, 2, 8); 
	
		PointDTO[] porintArr = new PointDTO[] {
				new CircleDTO(1, 2, 3),
				new CircleDTO(3, 3, 4),
				new RectangleDTO(-1, -2, 5, 2),
				new RectangleDTO(-2, 5, 2, 8)
		};
		
		int cnt1 = 0;
		int cnt2 = 0;
		for(PointDTO p : porintArr) {
			if(p != null) {
				if(p instanceof CircleDTO) {
					if(cnt1 == 0) {
						System.out.println("===== circle =====");
					}
					((CircleDTO) p).draw();
					cnt1++;
				}
				if(p instanceof RectangleDTO) {
					if(cnt2 == 0) {
						System.out.println("===== rectangle =====");
					}
					((RectangleDTO) p).draw();
					cnt2++;
				}
			}
		}
	}
	
}
