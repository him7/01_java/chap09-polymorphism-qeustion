package com.ohgiraffers.hw1.model.dto;

public class CircleDTO extends PointDTO {
	private int radius;
	
	public CircleDTO() {}

	public CircleDTO(int x, int y, int radius) {
		super(x, y);
		this.radius = radius;
	}
	
	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	@Override
	public void draw() {
		System.out.printf(
				"(x, y) : (%d, %d)\n면적 : %.1f\n둘레 : %.1f\n",
				super.x,
				super.y,
				(Math.PI * this.radius * this.radius),
				(Math.PI * this.radius * 2) 
		);
	}

}
