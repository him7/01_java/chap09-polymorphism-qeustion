package com.ohgiraffers.hw1.model.dto;

public class RectangleDTO extends PointDTO {
	private double width;
	private double height;
	
	public RectangleDTO() {}

	public RectangleDTO(int x, int y, double width, double height) {
		super(x, y);
		this.width = width;
		this.height = height;
	}
	
	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public void draw() {
		System.out.printf(
				"(x, y) : (%d, %d)\n면적 : %.1f\n둘레 : %.1f\n",
				super.x,
				super.y,
				(this.width * this.height),
				(this.width + this.height) * 2
		);
	}
	
}
